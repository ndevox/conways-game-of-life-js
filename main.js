import * as THREE from 'three';
import Grid from './conway/grid';

const SCENE = new THREE.Scene();
const CAMERA = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
CAMERA.position.z = 30;

const RENDERER = new THREE.WebGLRenderer();
RENDERER.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(RENDERER.domElement);


const animate = () => {
	requestAnimationFrame( animate );
	RENDERER.render( SCENE, CAMERA );
	let grid = new Grid();
	SCENE.add(grid);
	grid.populateGrid();
};

animate();
