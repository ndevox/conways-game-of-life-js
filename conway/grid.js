import * as THREE from "three";


export default class Grid extends THREE.Object3D {

    constructor() {
        super();
        this.grid = [];
    }

    createCube() {
        let geometry = new THREE.BoxGeometry( 1, 1, 0.4 );
        let material = new THREE.MeshBasicMaterial( { color: 0xffffff } );
        let cube = new THREE.Mesh( geometry, material );
        return cube
    };

    populateGrid() {
        let newGrid = [];
        for (let i = 0; i < 50; i++) {
            let row = [];
            for (let j = 0; j < 50; j++) {
                if ((j + i) % 2 === 0) {
                    let cube = this.createCube();
                    cube.position.x = i - 50 / 2;
                    cube.position.y = j - 50 / 2;
                    row.push(cube);
                    this.add(cube);
                }
            }
            newGrid.push(row);
        }
        this.grid = newGrid;
    }

}
